# soal-shift-sisop-modul-1-I01-2022

Group Members:

1. Denta Bramasta Hidayat (5025201116)
2. Muhammad Fatih Akbar (5025201117)
3. Muhammad Zufarrifqi Prakoso (5025201276)

## Important Links

- [Questions](https://docs.google.com/document/d/1qWo-VT3bYx8t9Bnpbq_h8Mpn9FpBM3t4T6SWX3vn9RI/edit?usp=sharing "Soal Shift 2s22")
- [Soal 1 Answer](https://gitlab.com/akbarfatih92/soal-shift-sisop-modul-2-i01-2022/-/blob/main/soal1/soal1.c)
- [Soal 2 Answer](https://gitlab.com/akbarfatih92/soal-shift-sisop-modul-2-i01-2022/-/blob/main/soal2/soal2.c)
- [Soal 3 Answer](https://gitlab.com/akbarfatih92/soal-shift-sisop-modul-2-i01-2022/-/tree/main/soal3)

## Table of Contents

- [soal-shift-sisop-modul-1-I01-2022](#soal-shift-sisop-modul-1-i01-2022)
  - [Important Links](#important-links)
  - [Table of Contents](#table-of-contents)
- [Soal 1](#soal-1)
  - [Question](#question)
  - [soal1.c](#soal1c)
  - [Explanation](#explanation)
    - [Starting & Ending Time (point e)](#starting--ending-time-point-e)
      - [Initializing the times](#initializing-the-times)
      - [get_epoch](#get_epoch)
      - [check_epoch](#check_epoch)
      - [Waiting until the time has come](#waiting-until-the-time-has-come)
    - [Creating the Daemon process](#creating-the-daemon-process)
      - [Output](#output)
    - [checkpid](#checkpid)
    - [Initializing the databases and the working directory (point a)](#initializing-the-databases-and-the-working-directory-point-a)
      - [init_databases](#init_databases)
      - [unzip_databases](#unzip_databases)
      - [rm_init_databases](#rm_init_databases)
      - [rename_numerical](#rename_numerical)
      - [rename_file](#rename_file)
      - [makedir](#makedir)
      - [Output](#output-1)
    - [Simulating the Gacha (point b,c,& d)](#simulating-the-gacha-point-bc-d)
      - [run_gacha](#run_gacha)
      - [count_files](#count_files)
      - [format_name](#format_name)
      - [format_gachatxt](#format_gachatxt)
      - [random_max](#random_max)
      - [get_json_value](#get_json_value)
      - [Output](#output-2)
    - [Zipping and Removing working directories (point e)](#zipping-and-removing-working-directories-point-e)
      - [zip_gacha](#zip_gacha)
      - [rm_all](#rm_all)
      - [Output](#output-3)
  - [Revisions Made](#revisions-made)
- [Soal 2](#soal-2)
  - [Question](#question-1)
  - [soal2.c](#soal2c)
  - [Explanation](#explanation-1)
    - [2a. Extract zip from drakor.zip to shift2/drakor and remove all its unnecessary folder](#2a-extract-zip-from-drakorzip-to-shift2drakor-and-remove-all-its-unnecessary-folder)
    - [2b. make genre folder from each file name](#2b-make-genre-folder-from-each-file-name)
    - [2c. rename the .png file to title only and then move to its genre folder](#2c-rename-the-png-file-to-title-only-and-then-move-to-its-genre-folder)
    - [2d. if poster have 2 picture then move .png file to each genre folder](#2d-if-poster-have-2-picture-then-move-png-file-to-each-genre-folder)
    - [2e. put drama's title and release year to data.txt and then sort ascendingly from its release years](#2e-put-dramas-title-and-release-year-to-datatxt-and-then-sort-ascendingly-from-its-release-years)
  - [Revisions Made](#revisions-made-1)
- [Soal 3](#soal-3)
  - [Question](#question-2)
  - [soal3.c](#soal3c)
  - [Explanation](#explanation-2)
    - [3a. create directory called "air" and "darat" in “/home/[USER]/modul2/”](#3a-create-directory-called-air-and-darat-in-homeusermodul2)
    - [3b. extract animal.zip](#3b-extract-animalzip)
    - [3c. separate the contents in the animal folder based on darat/air categories](#3c-separate-the-contents-in-the-animal-folder-based-on-daratair-categories)
    - [3d. Delete bird](#3d-delete-bird)
    - [3e. Create list.txt inside of air directory](#3e-create-listtxt-inside-of-air-directory)
  - [Revisions Made](#revisions-made-2)


# Soal 1

## Question

>Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat **sebuah program untuk men-simulasi sistem history gacha item pada game tersebut**. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, **bantulah mas Refadi untuk memenuhi keinginan nya itu.**

The question further explains on how the program must work, in summary these functions are as below:

+ Download & extracts the two "databases" from the following links
    + Database item characters :<br>
    `https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view`
    + Database item weapons :<br>
    `https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view`
+ Creates a folder name **gacha_gacha** which will be used as the working directory.
+ In this simulation, each run of gacha will cost an amount of **160 Primogems**, whereas we will start with having exactly **79000 Primogems**.
+ For every time the gacha runs, **a random** file from one of the two databases is selected and the values of the **name** and **rarity** are fetched from it. After which the fetched data will be appended into a .txt file with the format of **{jumlah-gacha}_{tipe-item}_{rarity}_{name}_{sisa-primogems}**.
+ Using the variable **jumlah_gacha**, which is the current number of attempt on the gacha program, there are three quirks that need to be implemented using this variable:
  + Whenever its value is odd, then draw the gacha from the **characters** database, otherwise draw from the **weapons** database
  + Whenever its value reached **modulo 10**, then a new .txt file is created with the filename of **{Hh:Mm:Ss}_gacha_{jumlah-gacha}** which the next data would be appended. Each .txt files are separated by a single second.
  + Whenever its value reached **modulo 90**, then a new folder would be created with the name of **total_gacha_{jumlah-gacha}** where new .txt files would be created.
  + In conclusion each .txt files would have **10 lines** of data insides, whereas all folders would have **9 .txt files** inside.
+ The gacha simulation should start automatically at exactly **30 March 04:44**. **3 hours** later, the **gacha_gacha** directory and its content will be compressed under the name of **not_safe_for_wibu** and password protected with ***"satuduatiga"***. Then all files used in the program will be deleted, leaving only the program itself and the zip file.
+ This program only uses **one script only** with it implementing a **Daemon process**.

## soal1.c

```c
#define _XOPEN_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <limits.h>
#include <time.h>
#include <dirent.h>
#include <json-c/json.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

#define GACHA_COST 160

// prints out a readable format from time_t
void check_epoch(time_t rawtime)
{
    struct tm * timeinfo;
    timeinfo = localtime(&rawtime);
    char buf[255];

    strftime(buf,sizeof(buf),"%Y-%m-%d %H:%M:%S",timeinfo);
    puts(buf);
}

// convert
time_t get_epoch(char *timestamp)
{
    struct tm timeinfo;
    memset(&timeinfo, 0, sizeof(timeinfo));
    strptime(timestamp, "%Y-%m-%d %H:%M:%S", &timeinfo);
    timeinfo.tm_isdst = -1;
    return mktime(&timeinfo);
}

// random number generator from 0 to max
int random_max(int max)
{
    int range = max;
    return rand() % range;
}

// JSON parser, gets the name and rarity values
void get_json_value(char *filename, char *name_val, char *rarity_val)
{
    FILE *fp;
    char buffer[4095];


    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;

    fp = fopen(filename,"r");
    fread(buffer, 4095, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"rarity",&rarity);
    json_object_object_get_ex(parsed_json,"name",&name);

    sprintf(name_val,"%s",json_object_get_string(name));
    sprintf(rarity_val,"%s",json_object_get_string(rarity));
}

// prevent any process other than the specified pid form passing this point
void checkpid(pid_t pid)
{
    if(getpid() != pid)
    {
        exit(EXIT_SUCCESS);
    }
}

// get gacha text content
void format_gachatxt(char *output, int dirsizes[] ,int jumlah_gacha, int sisa_primogems){
    char name[128];
    char rarity[8];
    char type[12];
    char fileloc[PATH_MAX];
    int numberoffiles;
    
    if(jumlah_gacha%2==0){
        strcpy(type,"weapon");
        numberoffiles = dirsizes[0];
    }else{
        strcpy(type,"character");
        numberoffiles = dirsizes[1];
    }

    sprintf(fileloc,"./%ss/%d.json",type,random_max(numberoffiles));
    // puts(fileloc); //debug only
    get_json_value(fileloc,name,rarity);
    sprintf(output,"%d_%s_%s_%s_%d\n",jumlah_gacha,type,rarity,name,sisa_primogems);
}

// get the filename format for the txt file which include a timestamp
void format_name(char *filename, char *foldername, int jumlah_gacha)
{
    time_t rawtime;
    struct tm * timeinfo;
    
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    sprintf(foldername, "gacha_gacha/total_gacha_%d",(jumlah_gacha/90)*90);
    sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt",foldername ,timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,(jumlah_gacha/10)*10);
}

// rename() substitute
void rename_file(char *oldname, char *newname)
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        // printf("was waiting for (%d) in rename on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        // printf("oldname: %s\nnewname: %s\n",oldname,newname); //debug only
        char *args[] = {"/bin/mv", "mv", oldname , newname, NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rename failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("rename failed!");
        exit(EXIT_FAILURE);
    }
}

// count all files that are in the directory
int count_files(char *dirpath)
{
    struct dirent *de;  
    int counter = 0;

    DIR *dr1 = opendir(dirpath);
  
    if (dr1 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr1)) != NULL) counter++;
    closedir(dr1); 

    return counter - 2;

}

// rename all files in the directory in an sequential number
void rename_numerical()
{
    struct dirent *de;  
    char fileloc[PATH_MAX];
    char newname[PATH_MAX];
    int counter = 0;

    DIR *dr1 = opendir("./weapons");
  
    if (dr1 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr1)) != NULL)
    {
        sprintf(fileloc,"./weapons/%s", de->d_name);
        if(strcmp(fileloc,"./weapons/.")!=0&&strcmp(fileloc,"./weapons/..")!=0)
        {
            sprintf(newname,"./weapons/%d.json", counter++);
            rename_file(fileloc,newname);
        }
        //sleep(0.10);
    }
  
    closedir(dr1);    

    counter = 0;

    DIR *dr2 = opendir("./characters");
  
    if (dr2 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr2)) != NULL)
    {
        sprintf(fileloc,"./characters/%s", de->d_name);
        if(strcmp(fileloc,"./characters/.")!=0&&strcmp(fileloc,"./characters/..")!=0)
        {
            sprintf(newname,"./characters/%d.json", counter++);
            rename_file(fileloc,newname);
        }
    }
  
    closedir(dr2);  
}

// mkdir() substitute
void makedir(char *namedir)
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        // printf("was waiting for (%d) in mkdir on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        char *args[5] = {"/bin/mkdir", "mkdir", "-p" , namedir, NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("mkdir failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// removes downloaded zip database
void rm_init_databases()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in rm_init on ppid of (%d)\n",waiter,getppid()); //debug only
        rename_numerical();
    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/rm", "rm", "-f", "db_weapons.zip", "db_characters.zip",NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rm failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// unzips downloaded zip database then forwards to rm_init_databases()
void unzip_databases()
{
    pid_t pid1,pid2,waiter;
    pid1 = fork();
    pid2 = fork();

    
     if(pid1 > 0 && pid2 > 0) 
    {
        // waiter = printf("unzip %d %d\n",pid1,pid2); //debug only
        wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        waiter =wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        //start
        rm_init_databases();
        //end
    }
    else if(pid1 == 0 && pid2 > 0)
    { 
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_weapons.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("unzip weapons failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_characters.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("unzip characters failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 < 0 && pid2 < 0)
    {
        perror("unzip childs failed!");
        exit(EXIT_FAILURE);
    }
}

// downloads the data into a zip file then forwards to unzip_databases()
void init_databases()
{
    pid_t pid1, pid2, waiter;
    pid1 = fork();
    pid2 = fork();

    if(pid1 > 0 && pid2 > 0) 
    {
        // printf("wget %d %d\n",pid1,pid2); //debug only
        waiter = wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        waiter = wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        //start
        unzip_databases();
        //end
    }
    else if(pid1 == 0 && pid2 > 0)
    { 
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "db_weapons.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("wget weapons failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "db_characters.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("wget characters failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 < 0 && pid2 < 0)
    {
        perror("wget childs failed!");
        exit(EXIT_FAILURE);
    }
}

// removes all working folders.
void rm_all()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in rm_all on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/rm", "rm", "-rf", "./weapons", "./characters", "./gacha_gacha",NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rmd failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// zips the gacha_gacha directory with password, then forwards to rm_all()
void zip_gacha()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in zip on ppid of (%d)\n",waiter,getppid()); //debug only
        rm_all();

    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/zip", "zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha/", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("zip failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// runs the gacha program
void run_gacha(){
    int curr_primogems = 79000;
    int jumlah_gacha = 0;
    char foldername[PATH_MAX];
    char filename[PATH_MAX];
    char gachabuffer[128];
    int dirsizes[2];
    dirsizes[0] = count_files("./weapons");
    dirsizes[1] = count_files("./characters");
    FILE *fp;


    do
    {
        format_name(filename,foldername,jumlah_gacha);

        if((jumlah_gacha%90)==0)
            makedir(foldername);

        fp = fopen(filename,"w");
        do
        {
            curr_primogems -= GACHA_COST;
            if(curr_primogems < 0)
                break;
            format_gachatxt(gachabuffer,dirsizes,jumlah_gacha,curr_primogems);
            //puts(gachabuffer); //debug only
            fputs(gachabuffer, fp);
            jumlah_gacha++;
        }
        while(jumlah_gacha%10!=0);
        sleep(1);
        fclose(fp);

        //if(jumlah_gacha >= 150)break; //debug only
        
    }
    while(curr_primogems > 0);
}

int main ()
{

    time_t start,end;
    start = get_epoch("2022-03-30 04:44:00");
    end = get_epoch("2022-03-30 07:44:00");
    pid_t daemon_pid,sid;

    // puts("start time:");
    // check_epoch(start);
    // puts("start time:");
    // check_epoch(end);

    // converting into daemon
    daemon_pid = fork();
    if (daemon_pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (daemon_pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);



    // get actual pid
    daemon_pid = getpid();

    checkpid(daemon_pid);

    // point a
    init_databases();
    checkpid(daemon_pid);
    
    makedir("gacha_gacha");
    
    // point b, c, & d
    checkpid(daemon_pid);
    while(time(NULL) < start); //waits until Wed Mar 30 2022 04:44:00 GMT+0700
    run_gacha();
    
    // point e
    checkpid(daemon_pid);
    while(time(NULL) < end); //waits until Wed Mar 30 2022 07:44:00 GMT+0700
    zip_gacha();

    // terminate remaining childs
    checkpid(daemon_pid);
    return 0;
}

```

## Explanation

### Starting & Ending Time (point e)

>*The gacha simulation should start automatically at exactly **30 March 04:44. 3 hours** later, the gacha_gacha directory and its content will be compressed ...*

#### Initializing the times

in the beginning of **main()**, this could be seen:

```c
time_t start,end;
start = get_epoch("2022-03-30 04:44:00");
end = get_epoch("2022-03-30 07:44:00");
```

Here it's initialized where the **run_gacha()** program should start and end, in which it's at **30 March 04:44** and ends **3 hours later** (30 March 07:44). To make it easier to use the time_t variable, the function **get_epoch** is used to convert string timestamp into time_t.

#### get_epoch 

```c
time_t get_epoch(char *timestamp)
{
    struct tm timeinfo;
    memset(&timeinfo, 0, sizeof(timeinfo));
    strptime(timestamp, "%Y-%m-%d %H:%M:%S", &timeinfo);
    timeinfo.tm_isdst = -1;
    return mktime(&timeinfo);
}
```

Using **strptime()** and the function's argument **char \*timestamp**, we can convert the timestamp string *(with the format of "%Y-%m-%d %H:%M:%S")* into a **tm** struct; After which since **strptime()** don't record wether the time use **daily light saving time (dst)**, and knowing here in Indonesia we don't use dst, an addition value is added int the **tm** struct `timeinfo.tm_isdst = -1;`. Lastly since we need a **time_t** rather than the struct, we can use **mktime()** to convert it.

#### check_epoch

Another function is used as to convert **time_t** into a timestamp string and print it out afterward. This function has only been used as a debugging tool.

```c
void check_epoch(time_t rawtime)
{
    struct tm * timeinfo;
    timeinfo = localtime(&rawtime);
    char buf[255];

    strftime(buf,sizeof(buf),"%Y-%m-%d %H:%M:%S",timeinfo);
    puts(buf);
}
```
The inner-working of this function is the opposite of that of **get_epoch**, where **mktime()** is replaced with **localtime()**, and **strptime()** is replaced with **strftime()**.

#### Waiting until the time has come

Further down in the **main()**, these two while loops can be seen:

```c
...

while(time(NULL) < start);

...

while(time(NULL) < end);

...
```

These empty while loops will hold the program until **time(NULL)** (the current time) has reached the time specified in **start** & **end**.

### Creating the Daemon process

> *This program only uses one script only with it implementing a **Daemon process**.*

This snippet of code is used in creating the whole process into a Daemon process:

```c
daemon_pid = fork();
if (daemon_pid < 0) {
    exit(EXIT_FAILURE);
}

if (daemon_pid > 0) {
    exit(EXIT_SUCCESS);
}

umask(0);

sid = setsid();

if (sid < 0) {
    exit(EXIT_FAILURE);
}

close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
```

Nothing that much of difference than any other Daemon Process creation, starting from **forking** the process and exiting the parent process, setting its **umask** as **0**, so it can read, write, and execute, setting the process with a new unique **SID**, and closing its **standard file descriptor**. The only thing to notice is that the working directory is kept, this is to fully utilize the multiple **execv** in the program without adding **getcwd()** into the mix.


#### Output

using `ps -aux | grep "soal1"` to check if the process is running

![ps -aux output](https://i.imgur.com/5mU7j5n.png)

### checkpid

The **checkpid()** function is used as a fail-safe to prevent any runaway processes that may/should have been exited previously.

```c
void checkpid(pid_t pid)
{
    if(getpid() != pid)
    {
        exit(EXIT_SUCCESS);
    }
}
```

This is done by exiting any other processes that don't have the same **pid** as the argument of the function, which in most cases will be the current parent process.

An example use of this can be seen as below:

```c
daemon_pid = getpid();

checkpid(daemon_pid);
```

### Initializing the databases and the working directory (point a)

> *Download & extracts the **two "databases"** from the following link ... Creates a folder name **gacha_gacha** which will be **used as the working directory.***

In the **main()**, we can see the following snippet:

```c
init_databases();
checkpid(daemon_pid);

makedir("gacha_gacha");
```

The **init_databases()** function will download, extracts, remove the leftover zip files, and rename the files of the database to ease the random file picker, hence it *initializes* the database. The **makedir()** function is used as a substitute for **mkdir()**, as so it's used to create new a directory.

#### init_databases

```c
void init_databases()
{
    pid_t pid1, pid2, waiter;
    pid1 = fork();
    pid2 = fork();

    if(pid1 > 0 && pid2 > 0) 
    {
        // printf("wget %d %d\n",pid1,pid2); //debug only
        waiter = wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        waiter = wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        //start
        unzip_databases();
        //end
    }
    else if(pid1 == 0 && pid2 > 0)
    { 
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "db_weapons.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("wget weapons failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "db_characters.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("wget characters failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 < 0 && pid2 < 0)
    {
        perror("wget childs failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function specifically is used for downloading the two databases from their respective Google Drive link using **wget** via **execv()**. As to use the **execv()** function, a new process is created to facilitate them; And since there will be 2 **wget** commands, two processes are created as below:

```c
pid_t pid1, pid2, waiter;
pid1 = fork();
pid2 = fork();
```

after which a set of if statements can be seen, these are:

+ `if(pid1 > 0 && pid2 > 0)`, the parent process will reside here.<br/>
The parent would wait for the two child processes to exit via the two `wait(NULL);` before proceeding to the next function, **unzip_databases()**.
+ `else if(pid1 == 0 && pid2 > 0)`, the first process will reside here
+ `else if(pid1 > 0 && pid2 == 0)`, the second process will reside here
+ `else if(pid1 < 0 && pid2 < 0)`, if any of the two processes failed to be created. <br/> If so the process would be exited, stopping the whole program.

Inside both process the coding are mostly the same with the difference of the link used as in:

```c
char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&[GOOGLE DRIVE ID HERE]", "-O", "db_[ITEM TYPE HERE].zip", NULL};
if(execv(args[0],args+1) == -1) 
{
    perror("wget [ITEM TYPE HERE] failed!");
    exit(EXIT_FAILURE);
}
```

For the arguments for the **execv** both calls for the **wget** program while using the **--quiet** flag in order to not fill up the terminal when debugging and **--no-check-certificate** flag to ignore all certification warning. For the links, since the "raw" Google Drive link couldn't be used directly, the link have been modified to compensate by taking the IDs (ex: ht<span>tps://drive.google</span>.com/file/d/**1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT**/view) and appending that onto `https://docs.google.com/uc?export=download&`. This is then outputted as a .zip file with their respective names.

The above-mentioned is putted into a string array **char \*args[]** in which it is inputted into the **execv** function. And If statement surrounding the **execv** is putted to catch for any errors and stopping the program if it happens to find one.

#### unzip_databases

```c
void unzip_databases()
{
    pid_t pid1,pid2,waiter;
    pid1 = fork();
    pid2 = fork();

    
     if(pid1 > 0 && pid2 > 0) 
    {
        // waiter = printf("unzip %d %d\n",pid1,pid2); //debug only
        wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        waiter =wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        //start
        rm_init_databases();
        //end
    }
    else if(pid1 == 0 && pid2 > 0)
    { 
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_weapons.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("unzip weapons failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_characters.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("unzip characters failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 < 0 && pid2 < 0)
    {
        perror("unzip childs failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function specifically is used for extracting the just been downloaded databases using **unzip** via **execv()**. Compared to the previous function, there's nothing much of difference excepting its **char \*args[]** which are :

```c
char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_[ITEM TYPE HERE].zip", NULL};
```

This would make **execv** calls for the **unzip** program with the **-qq** flag in order to run the command super quietly and the **-n** flag to ignore if an extracted folder with the same name is present.

After the parent process finished waiting for the two child processes, it will then proceed to the **rm_init_databases()** function.

#### rm_init_databases

```c
void rm_init_databases()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in rm_init on ppid of (%d)\n",waiter,getppid()); //debug only
        rename_numerical();
    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/rm", "rm", "-f", "db_weapons.zip", "db_characters.zip",NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rm failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function is used to remove the leftover .zip files which are still there after unzipping using **rm** via **execv**. Compared to the previous two function, again nothing much of difference excepting the **char \*args[]** and that it only have a single child process made.

The arguments is as follow:
```c
char *args[] = {"/bin/rm", "rm", "-f", "db_weapons.zip", "db_characters.zip",NULL};
```

This make **execv** calls for the **rm** program with the flag **-f** as to ignore when the file does not exist, in which it then removes the two .zip files.

After the parent process finished waiting for the child process, it will then proceed to the rename_numerical() function.

#### rename_numerical

This function is used to rename all files in both databases into a sequentially incrementing number starting from 0, as to ease the use of the random file picker in later functions. 

```c
void rename_numerical()
{
    struct dirent *de;  
    char fileloc[PATH_MAX];
    char newname[PATH_MAX];
    int counter = 0;

    DIR *dr1 = opendir("./weapons");
  
    if (dr1 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr1)) != NULL)
    {
        sprintf(fileloc,"./weapons/%s", de->d_name);
        if(strcmp(fileloc,"./weapons/.")!=0&&strcmp(fileloc,"./weapons/..")!=0)
        {
            sprintf(newname,"./weapons/%d.json", counter++);
            rename_file(fileloc,newname);
        }
        //sleep(0.10);
    }
  
    closedir(dr1);    

    counter = 0;

    DIR *dr2 = opendir("./characters");
  
    if (dr2 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr2)) != NULL)
    {
        sprintf(fileloc,"./characters/%s", de->d_name);
        if(strcmp(fileloc,"./characters/.")!=0&&strcmp(fileloc,"./characters/..")!=0)
        {
            sprintf(newname,"./characters/%d.json", counter++);
            rename_file(fileloc,newname);
        }
    }
  
    closedir(dr2);  
}
```

By utilizing the **<dirent.h>** library, we are able to read through contents of the directory. This is done by first opening the directory via `opendir()` and then storing into a `DIR` variable. After making sure that the program is able to open the directory, a while loop can be seen using `(de = readdir(DIR)) != NULL`, this will make sure it will keep looping until all contents have been read (*note: this can only be done on POSIX or Linux systems*). 

Inside the while loop, a **sprintf** is use to transfer the file name and appending it onto the parent directory which will be put onto **fileloc**. One thing to note is that since **"."** and **".."** are part of the contents inside each and every directory, we would need to ignore these "filenames" by using an If statement with **strcmp** for both "filenames". If it passes the **strcmp**, then a **newname** is created by using the number from **counter** for its name, then using **rename_file** function we cane rename the file from the **fileloc** into **filename**.

This are done two times for each databases, in which when between the two the **counter** will be reset into 0.

#### rename_file

```c
void rename_file(char *oldname, char *newname)
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        // printf("was waiting for (%d) in rename on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        // printf("oldname: %s\nnewname: %s\n",oldname,newname); //debug only
        char *args[] = {"/bin/mv", "mv", oldname , newname, NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rename failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("rename failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function is used to rename a file from a file location string ,**oldname** and changing that with another file location string, **newname**; this is done by using **mv** via **execv**. This function similar with other **"execv based"** functions such as **rm_init_database()**, in which the only real difference is the **char \*args[]** where is:

```c
char *args[] = {"/bin/mv", "mv", oldname , newname, NULL};
```

This make the **execv** calls for the **mv** program, and use the both function argument as the argument for the **mv**.

#### makedir

```c
void makedir(char *namedir)
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        // printf("was waiting for (%d) in mkdir on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        char *args[5] = {"/bin/mkdir", "mkdir", "-p" , namedir, NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("mkdir failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function is used to create a new directory using **mkdir** via **execv**. Like any other **"execv based"** functions, the only real difference is its **char \*args[]** which is:

```c
char *args[5] = {"/bin/mkdir", "mkdir", "-p" , namedir, NULL};
```
This make **execv** calls for the **mkdir** program with the flag **-p** to ignore if a directory with the same name as **namedir** is presents.

#### Output

The created database and working directory:

![Initialized database and working directory output](https://i.imgur.com/JP9ihwc.png)

The newly renamed character database content:

![character database](https://i.imgur.com/rS4Dgex.gif)

The newly renamed weapon database content:

![weapon database](https://i.imgur.com/IhlFBa1.gif)

### Simulating the Gacha (point b,c,& d)

In the **main()**, this could be seen:

```c
checkpid(daemon_pid);
while(time(NULL) < start); //waits until Wed Mar 30 2022 04:44:00 GMT+0700
run_gacha();
```

After exiting all runaway processes, and waiting until the time has come, this will run **run_gacha()** function which is the function that simulates gacha and saves the data into their respective files and directories.

#### run_gacha

```c
void run_gacha(){
    int curr_primogems = 79000;
    int jumlah_gacha = 0;
    char foldername[PATH_MAX];
    char filename[PATH_MAX];
    char gachabuffer[128];
    int dirsizes[2];
    dirsizes[0] = count_files("./weapons");
    dirsizes[1] = count_files("./characters");
    FILE *fp;

    do
    {
        format_name(filename,foldername,jumlah_gacha);

        if((jumlah_gacha%90)==0)
            makedir(foldername);

        fp = fopen(filename,"w");
        do
        {
            curr_primogems -= GACHA_COST;
            if(curr_primogems < 0)
                break;
            format_gachatxt(gachabuffer,dirsizes,jumlah_gacha,curr_primogems);
            //puts(gachabuffer); //debug only
            fputs(gachabuffer, fp);
            jumlah_gacha++;
        }
        while(jumlah_gacha%10!=0);
        sleep(1);
        fclose(fp);

        //if(jumlah_gacha >= 150)break; //debug only
        
    }
    while(curr_primogems > 0);
}
```

First, multiple variables are created of which are:

+ `int curr_primogems = 79000;`, states the current number of currency the simulation have
+ `int jumlah_gacha = 0;`, states the number of runs or in other word a counter
+ `char foldername[PATH_MAX];`, states the name of the current directory where the .txt file should be placed.
+ `char filename[PATH_MAX];`, states the name of the current .txt files of where the data should be appended to.
+ `char gachabuffer[128];`, is where the data would be placed before appending it onto the .txt file.
+ `int dirsizes[2];`, states the number of files contains to each database, this is done using the **count_files** function.
+ `FILE *fp`, a file pointer, used for any file operation.
+ also, `#DEFINE GACHA_COST 160` outside the function, use to define the cost for each run.

Next going inside a while loop that stops when the **curr_primogems** reach below zero, we will find `format_name(filename,foldername,jumlah_gacha);` which will fill **filename** & **foldername** with their respective name as stated in the question. then using an if statement and **makedir()**, the function will create a new directory with the name of **foldername** for every time the **jumlah_gacha** is modulo 90. And then a `fp = fopen(filename,"w");` will create a file with the name of **filename** and open it for appending purpose.

Going inside the next while loop with the condition of `jumlah_gacha%10!=0` as so each .txt files have 10 lines of data, the first thing is to subtract the **curr_primogems** with **GACHA_COST** and then check whether the curr_primogems still suffice. If it suffices, then we can get the data from a random files using the **format_gachatxt** function which will transfer its data into **gachabuffer** in which this buffer would then be appended onto the .txt files via **fputs()**.  After that the **jumlah_gacha** counter is incremented to indicate a gacha run has finished.

Since each .txt files are separated by a single second, a **sleep(1)** is used to delay it by a second. After which the current file pointer is closed to make it available for the next file.

#### count_files

```c
int count_files(char *dirpath)
{
    struct dirent *de;  
    int counter = 0;

    DIR *dr1 = opendir(dirpath);
  
    if (dr1 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr1)) != NULL) counter++;
    closedir(dr1); 

    return counter - 2;

}
```

This function is used to count the number of contents in a directory (**dirpath**) by using **readdir()** similar to that as **rename_numerical**. For every content, the **counter** is increment, this later on be use as the return value minus **"."** and **".."** hence it's subtracted by 2.

#### format_name

```c
void format_name(char *filename, char *foldername, int jumlah_gacha)
{
    time_t rawtime;
    struct tm * timeinfo;
    
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    sprintf(foldername, "gacha_gacha/total_gacha_%d",(jumlah_gacha/90)*90);
    sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt",foldername ,timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,(jumlah_gacha/10)*10);
}
```

This function is used to get the names for the .txt file and folder at the same time. The folder uses `total_gacha_{jumlah-gacha}` format, as so it uses a **sprintf** to **foldername** with only **jumlah_gacha** and **gacha_gacha/** (working directory) added on front. The .txt file uses `{Hh:Mm:Ss}_gacha_{jumlah-gacha}` format, as so it uses a **sprintf** to **filename** with a timestamp using the tm **struct** from `time(&rawtime); timeinfo = localtime(&rawtime);`, **jumlah_gacha** and **foldername** added in the front to indicate where the .txt file should be located.

#### format_gachatxt
```c
void format_gachatxt(char *output, int dirsizes[] ,int jumlah_gacha, int sisa_primogems){
    char name[128];
    char rarity[8];
    char type[12];
    char fileloc[PATH_MAX];
    int numberoffiles;
    
    if(jumlah_gacha%2==0){
        strcpy(type,"weapon");
        numberoffiles = dirsizes[0];
    }else{
        strcpy(type,"character");
        numberoffiles = dirsizes[1];
    }

    sprintf(fileloc,"./%ss/%d.json",type,random_max(numberoffiles));
    // puts(fileloc); //debug only
    get_json_value(fileloc,name,rarity);
    sprintf(output,"%d_%s_%s_%s_%d\n",jumlah_gacha,type,rarity,name,sisa_primogems);
}
```

This function is used to get the data and format it up onto a string file, in which will be used in **run_gacha()**. The first thing that can be seen is an if-else that is used determine whether **jumlah_gacha** is odd or even, if even then it will pick a file from the weapons type, else from the characters type.
Next a **fileloc** will be determined via the **type** that has been chosen and a random number using **random_max(numberoffiles)** which will pick a number from 0 to **numberoffiles**. 

This **fileloc** which is a .json file, would need to be parsed in order to get the only value we need, **name** & **rarity**. This can be done using **get_json_value()** in which it will transfer the values onto **rarity** and **name**. After which a **sprintf** is used to put the data into format which is `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}` and return the string.

#### random_max

```c
int random_max(int max)
{
    int range = max;
    return rand() % range;
}
```

This function is used to get a random number from 0 to **range**, this is done using a **rand()** and a modulo.

#### get_json_value

```c
void get_json_value(char *filename, char *name_val, char *rarity_val)
{
    FILE *fp;
    char buffer[4095];


    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;

    fp = fopen(filename,"r");
    fread(buffer, 4095, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"rarity",&rarity);
    json_object_object_get_ex(parsed_json,"name",&name);

    sprintf(name_val,"%s",json_object_get_string(name));
    sprintf(rarity_val,"%s",json_object_get_string(rarity));
}
```

This function is used to parse a .json file and extract its **name** & **rarity** values. This is done using the **<json-c/json.h>** library which have been installed externally. After reading the .json file, a **json_object_object_get_ex()** is used to extract the values onto their respective **json_object** struct from the **parsed_json** struct, then extracting the string using **json_object_get_string()** and transfer it via **sprintf**.

#### Output

After simulating the gacha until hitting 0 **Primogems**, below is the content of **gacha_gacha**.

![gacha_gacha content](https://i.imgur.com/gVN9srF.png)

A content of one of its folders.

![total_gacha_270 content](https://i.imgur.com/ypn9lQJ.png)

A content of one of its .txt files.

![.txt file content](https://i.imgur.com/iof67lS.png)

### Zipping and Removing working directories (point e)

In the **main()**, we can see:

```c
checkpid(daemon_pid);
while(time(NULL) < end); //waits until Wed Mar 30 2022 07:44:00 GMT+0700
zip_gacha()
```
After exiting all runaway processes, and waiting until the time has come, this will run **zip_gacha()** which will zip the **gacha_gacha** directory including all of its contents, in which after it will proceed to remove the **./weapons**, **./characters**, and the leftover **./gacha_gacha** directory and all of its content.

#### zip_gacha

```c
void zip_gacha()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in zip on ppid of (%d)\n",waiter,getppid()); //debug only
        rm_all();

    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/zip", "zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha/", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("zip failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function is specifically used to zip the directory and the contents of **gacha_gacha** using **zip** via **execv**. Like any other **"execv based"** functions, the only real difference is its **char \*args[]** which is:

```c
char *args[] = {"/bin/zip", "zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha/", NULL};
```

This make **execv** call for the **zip** program with the **-rq** flag as so it recursively adds all content of the directory while also doing it quietly, and the **--password satuduatiga** flag to password protect zip file with **not_safe_for_wibu.zip** as its filename. After the child process has exited, it will proceed onto the **rm_all** function

#### rm_all

```c
void rm_all()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in rm_all on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/rm", "rm", "-rf", "./weapons", "./characters", "./gacha_gacha",NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rmd failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}
```

This function is used to remove the **./weapons**, **./characters**, and the leftover **./gacha_gacha** directory and all of its content using **rm** via **execv**. Like any other **"execv based"** functions, the only real difference is its **char \*args[]** which is:

```c
char *args[] = {"/bin/rm", "rm", "-rf", "./weapons", "./characters", "./gacha_gacha",NULL};
```

This make **execv** calls for the **rm** program with the **-rf** flag as in recursively force, and the name of the directories to be removed.

#### Output

Once running the **zip_gacha()** and **rm_all()**, what is left is as below.

![Only zip file left](https://i.imgur.com/D2fyH90.png)

**not_safe_for_wibu.zip** content.

![zip file content](https://i.imgur.com/1mUbpVG.png)

Each file is password protected.

![Password-protection](https://i.imgur.com/DiV8SGT.png)

## Revisions Made

+ Added **check_epoch()** and **get_epoch()** in order to make reading the **time_t** variable easier. Please refer to commit [cbe42db3](https://gitlab.com/akbarfatih92/soal-shift-sisop-modul-2-i01-2022/-/commit/cbe42db3049f1f94217750a37f76c8eca950196d)

# Soal 2

## Question

- Extract zip from `drakor.zip` to `shift2/drakor` and remove all its unnesecary folder
- make genre folder from each file name
- rename the `.png` file to title only and then move to its genre folder
- if poster have 2 picture then move `.png` file to each genre folder
- put drama's title and release year to data.txt and then sort ascendingly from its release years

## soal2.c

```c
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>

typedef struct
{
    char title[50];
    int year;
} Data;
int compare (const void * a, const void * b)
{

  Data *dataA = (Data *)a;
  Data *dataB = (Data *)b;

  return ( dataA->year - dataB->year );
}
void dirmake(char *dest){
    char *argv[] = {"mkdir", "-p", dest, NULL};
    execv("/bin/mkdir", argv);
}
void unzipdrakor(char *zip, char *dest){
    char *unzip[] = {"unzip", zip, "-d", dest,"*.png", NULL};
    execv("/bin/unzip", unzip);
}
void norepeatfork (char execute[], char *variable_path[]) 
{ 
    int status; 
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0); 
    } 
}

//make a function that parsing string with underscore
char *parseString(char *s,int i){
    char *token;
    char *delimiter = ";";
    int var= 0;
    token = strtok(s,delimiter);
    while(var++ != i){
        token = strtok(NULL,delimiter);
    }
    if(strstr(token,".") == NULL)
        return token;
    return strtok(token,".");
}
void moveFile(char *src, char *dest){
    char *argv[] = {"mv", src, dest, NULL};
    execv("/bin/mv", argv);
}
void copyFile(char *src, char *dest){
    char *argv[] = {"cp", src, dest, NULL};
    execv("/bin/cp", argv);
}
void movegenre(char *directory){
    DIR *dir;
    pid_t child_id;
    int status;
    struct dirent *ent;
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            if(strstr(ent->d_name,".png") != NULL){
                char dest[300],dest2[300];
                char src[300];
                if(strstr(ent->d_name,"_") == NULL){
                    sprintf(src,"%s/%s",directory,ent->d_name);
                    char title[50];
                    strcpy(title,ent->d_name);
                    sprintf(dest,"%s/%s/%s",directory,parseString(title,2),ent->d_name);
                    child_id = fork();
                    if(child_id < 0)
                    {
                        exit(EXIT_FAILURE);
                    }
                    if(child_id == 0){
                        moveFile(src,dest);
                    }
                    else{
                        while ((wait(&status)) > 0);
                    }
                }
                else{
                    char *delimiter = "_";

                    sprintf(src,"%s/%s",directory,ent->d_name);
                    char title1[50];
                    char title2[50];
                    char *token = strtok(ent->d_name,delimiter);
                    char *token2 = strtok(NULL,delimiter);
                    strcpy(title1,token);
                    sprintf(dest,"%s/%s/%s.png",directory,parseString(title1,2),token);
                    strcpy(title2,token2);
                    sprintf(dest2,"%s/%s/%s",directory,parseString(title2,2),token2);
                    char *argv[] = {"cp", src, dest, NULL};
                    norepeatfork("/bin/cp", argv);
                    char *argv2[] = {"cp", src, dest2, NULL};
                    norepeatfork("/bin/cp", argv2);
                    char *argv3[] = {"rm", src, NULL};
                    norepeatfork("/bin/rm", argv3);
                }
            }
        }
    }
}

void removefile(){
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id == 0)
    {
        char *argv[] = {"rm", "/home/zufarrifqi/shift2/data.txt", NULL};
        execv("/bin/rm", argv);
    }
    else
    {
        ((wait(&status))>0);
        char *argv[] = {"rm", "/home/zufarrifqi/shift2/drakor/data.txt", NULL};
        execv("/bin/rm", argv);
    }
}
void makeFile(char* directory, Data data[],int count,char *genre){
    FILE *fptr;
    char direct[200];
    sprintf(direct,"%s",directory);
    chdir(direct);

    
    fptr = fopen("data.txt","w");
    //fprintf genre to data txt
    fprintf(fptr,"Kategori : %s\n",genre);
    //write data to data.txt
    qsort(data,count,sizeof(Data),compare);
    for(int i = 0; i < count; i++){ 
        fprintf(fptr,"\nnama : %s\n",data[i].title);
        fprintf(fptr,"rilis : tahun %d\n",data[i].year);
    }
    fclose(fptr);
    // removefile();
 }
void makeData(char *directory,char * genre){
    DIR *dir;
    pid_t child_id;
    int status;
    struct dirent *ent;
    int count = 0;
    Data data[100];
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            if(strstr(ent->d_name,".png") != NULL){
                char title[300];
                char src[300];
                char *delimiter = ";.";

                sprintf(src,"%s/%s",directory,ent->d_name);
                char *token = strtok(ent->d_name,delimiter);
                strcpy(data[count].title,token);
                // printf("%s\n",data[count].title);
                sprintf(title,"%s/%s.png",directory,token);
                token = strtok(NULL,delimiter);
                
                data[count].year = atoi(token);
                // printf("%d\n",data[count].year);
                // token = strtok(NULL,delimiter);
                // printf("%s\n",token);
                count++;
                child_id = fork();
                if(child_id < 0)
                {
                    exit(EXIT_FAILURE);
                }
                if(child_id == 0){
                    char *argv[] = {"mv",src,title, NULL};
                    execv("/bin/mv", argv);
                }
                else{
                    while ((wait(&status)) > 0);
                }
            }
            
        }
    }
    makeFile(directory,data,count,genre);
    memset(data,0,sizeof(Data)*100);
}
void changedir(char *directory){
    DIR *dir;
    struct dirent *ent;
    int files = 0;
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            char dest[300];
            sprintf(dest,"%s/%s",directory,ent->d_name);  
            makeData(dest,ent->d_name);
        }
    }
}
void mkdirgenre(char *directory){
    DIR *dir;
    struct dirent *ent;
    dir = opendir (directory);
    int status;
    pid_t child_id;
    char genredir[100],genre[10][100];
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            child_id = fork();
            if(child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            if(child_id == 0){
                if(strstr(ent->d_name,"_") == NULL){
                    sprintf(genredir,"%s/%s",directory,parseString(ent->d_name,2));
                    dirmake(genredir);
                }
                else{
                    char *delimiter = "_";
                    char *token;
                    token = strtok(ent->d_name,delimiter);
                    child_id = fork();
                    if(child_id < 0)
                    {
                        exit(EXIT_FAILURE);
                    }
                    if(child_id == 0){
                        sprintf(genredir,"%s/%s",directory,parseString(token,2));
                        dirmake(genredir);
                    }

                }
            } 
            else{
                while ((wait(&status)) > 0);

            }
        }
        closedir(dir);
    }
    else perror ("Couldn't open the directory");
}

int main(){
    int status;
    char dest[100] = "/home/zufarrifqi/shift2/drakor";
    char zip[100] = "/home/zufarrifqi/drakor.zip";
    pid_t child_id;
    child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        dirmake(dest);
    }
    else{
        while ((wait(&status)) > 0);
        child_id = fork();
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }
        if(child_id == 0){
            unzipdrakor(zip,dest);
        }
        else{
            while ((wait(&status)) > 0);
            child_id = fork();
            if (child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            if(child_id == 0){
                mkdirgenre(dest);
            }
            else{
                while ((wait(&status)) > 0);
                movegenre(dest);
                changedir(dest);
                removefile();
                }

            }
        }
        
    }

```

## Explanation
### 2a. Extract zip from drakor.zip to shift2/drakor and remove all its unnecessary folder
First of all i create path for shift2/drakor in `dest` and then use mkdir in `dirmake()` to make new directory
```c
void dirmake(char *dest){
    char *argv[] = {"mkdir", "-p", dest, NULL};
    execv("/bin/mkdir", argv);
}
```
call the function in main 
```c
dirmake(dest);
```
To unzip file i create the path for `drakor.zip` location and  using unzip in `unzipdrakor`. To remove all its unnecessary file i make the `argv` so that it only unzip `.png` file.
```c
void unzipdrakor(char *zip, char *dest){
    char *unzip[] = {"unzip", zip, "-d", dest,"*.png", NULL};
    execv("/bin/unzip", unzip);
}
```
call the function in main
```c
unzipdrakor(zip,dest);
```
Unzip Result :

![list](https://i.imgur.com/vV0AzrB.png)
### 2b. make genre folder from each file name
To make the folder of each genre from the file, i take the name of each png file using `ent->d_name` and then take last substring from the name of the file. To do that i use `parsestring()` function. How this function work is that it create token with `;` delimiter to seperate where is title,release year and genre.
```c
char *parseString(char *s,int i){
    char *token;
    char *delimiter = ";";
    int var= 0;
    token = strtok(s,delimiter);
    while(var++ != i){
        token = strtok(NULL,delimiter);
    }
    if(strstr(token,".") == NULL)
        return token;
    return strtok(token,".");
}
```
Because some of the file may have 2 posters, i create 2 condition where if the file only have 1 poster, it will parsestring first and then directly make directory for genre. If the files have 2 posters it will create token with `_` delimiter. Use parsestring on token and then make the directory. 
```c
void mkdirgenre(char *directory){
    DIR *dir;
    struct dirent *ent;
    dir = opendir (directory);
    int status;
    pid_t child_id;
    char genredir[100],genre[10][100];
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            child_id = fork();
            if(child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            if(child_id == 0){
                if(strstr(ent->d_name,"_") == NULL){
                    sprintf(genredir,"%s/%s",directory,parseString(ent->d_name,2));
                    dirmake(genredir);
                }
                else{
                    while ((wait(&status)) > 0);
                    char *delimiter = "_";
                    char *token;
                    token = strtok(ent->d_name,delimiter);
                    child_id = fork();
                    if(child_id < 0)
                    {
                        exit(EXIT_FAILURE);
                    }
                    if(child_id == 0){
                        sprintf(genredir,"%s/%s",directory,parseString(token,2));
                        dirmake(genredir);
                    }

                }
            } 
            else{
                while ((wait(&status)) > 0);

            }
        }
        closedir(dir);
    }
    else perror ("Couldn't open the directory");
}
```
Result : 

![fold](https://i.imgur.com/3mi7FFb.png)

### 2c. rename the .png file to title only and then move to its genre folder
First of all we need to read directory using readdir and then read all the .png file. To move the file we need to know where to move it.  because of that we using `sprintf`to `src` with `parsestring` so that we can read the genre on each file and then move it to the correct folder.
```c
 while ((ent = readdir(dir)) != NULL){
    if(strstr(ent->d_name,".png") != NULL){
        char dest[300],dest2[300];
        char src[300];
        if(strstr(ent->d_name,"_") == NULL){
            sprintf(src,"%s/%s",directory,ent->d_name;
            char title[50];
            strcpy(title,ent->d_name);
            sprintf(dest,"%s/%s/%s",directory parseString(title,2),ent->d_name);
            child_id = fork();
            if(child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            if(child_id == 0){
                moveFile(src,dest);
            }
            else{
                while ((wait(&status)) > 0);
            }
        }
    }
}
```
if data from the file have been input to data.txt and then rename file to title only in `makedata` which i will discuss in 2e
```c
sprintf(src,"%s/%s",directory,ent->d_name);
char *token = strtok(ent->d_name,delimiter);        
sprintf(title,"%s/%s.png",directory,token);
if(child_id == 0){
    char *argv[] = {"mv",src,title, NULL};
    execv("/bin/mv", argv);
}
```
### 2d. if poster have 2 picture then move .png file to each genre folder
The same as making directory for genre, we use token with `_` delimiter and then parse the string. Instead of using `mv` to execv, we use cp and we copy the file to each genre folder. If all were done we remove the file in shift2/drakor.
```c
char *delimiter = "_";

sprintf(src,"%s/%s",directory,ent->d_name);
char title1[50];
char title2[50];
char *token = strtok(ent->d_name,delimiter);
char *token2 = strtok(NULL,delimiter);
strcpy(title1,token);
sprintf(dest,"%s/%s/%s.png",directory,parseString(title1,2),token);
strcpy(title2,token2);
sprintf(dest2,"%s/%s/%s",directory,parseString(title2,2),token2);
char *argv[] = {"cp", src, dest, NULL};
norepeatfork("/bin/cp", argv);
char *argv2[] = {"cp", src, dest2,NULL};
norepeatfork("/bin/cp", argv2);
 char *argv3[] = {"rm", src, NULL};
norepeatfork("/bin/rm", argv3);
```
Same as 2c we rename the file to title only in `makedata`

Result of `movegenre` (Action and romance folder) :

![action](https://i.imgur.com/aUW95A1.png)

![romance](https://i.imgur.com/D7vnJSk.png)

![drakor](https://i.imgur.com/U8rl2xi.png)

As you can see the all of png already moved and separated


### 2e. put drama's title and release year to data.txt and then sort ascendingly from its release years
First of all what i do is we need to read all directory in shift2/drakor
```c
void changedir(char *directory){
    DIR *dir;
    struct dirent *ent;
    int files = 0;
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            char dest[300];
            sprintf(dest,"%s/%s",directory,ent->d_name);  
            makeData(dest,ent->d_name);
        }
    }
}
```
what i do is that i use opendir in shift2/drakor and then read one by one of the genre folder

To get all of the value from name of file we need to input all of value from name of the file using struct. so that we can input drama title and release year.
```c
typedef struct
{
    char title[50];
    int year;
} Data;
```
we create token with delimiter `;.` and then input all drama title and release year.
```c
if(strstr(ent->d_name,".png") != NULL){
  char title[300];
  char src[300];
  char *delimiter = ";.";
  char *token = strtok(ent->d_name,delimiter);
  strcpy(data[count].title,token);
  token = strtok(NULL,delimiter);           
  data[count].year = atoi(token);
  count++;
```
To create data.txt i use `makefile` function. I use qsort to sort ascendingly from data we got and then write the release year and title to data.txt using `fprintf`.
```c
void makeFile(char* directory, Data data[],int count,char *genre){
    FILE *fptr;
    char direct[200];
    sprintf(direct,"%s",directory);
    chdir(direct);

    
    fptr = fopen("data.txt","w");
    //fprintf genre to data txt
    fprintf(fptr,"Kategori : %s\n",genre);
    //write data to data.txt
    qsort(data,count,sizeof(Data),compare);
    for(int i = 0; i < count; i++){ 
        fprintf(fptr,"\nnama : %s\n",data[i].title);
        fprintf(fptr,"rilis : tahun %d\n",data[i].year);
    }
    fclose(fptr);
    // removefile();
 }
```
Data.txt result on action folder :
![data](https://i.imgur.com/0I3NyPt.png)

Final result after renaming file :
![rename](https://i.imgur.com/50N3yAo.png)
## Revisions Made
- fix the parsestring function and dirmake fucntion where some of the folder cant be made
- added solution to question 2c - 2e
# Soal 3

## Question

in question number 2 we create a .c file which contains about creating air and darat directory files, then after that extract animal.zip and then separate the contents in the animal folder based on darat/air categories. after that we delete all files that are categorized as "bird" because the birds in the zoo are full. then lastly we create a list.txt in the water folder with the format list of UID or user names and also the permissions of the user.

## soal3.c

```c
/*
Denta Bramasta Hidayat
5025201116
I01
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <syslog.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>

void norepeatfork (char execute[], char *variable_path[])
{
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0);
        return;
    }
}

void Unzip(){
    char destination[101] = "/Users/dentabramasta/Modul2/soal3";
    char darat[101] = "/Users/dentabramasta/Modul2/soal3/darat";
    char air[101] = "/Users/dentabramasta/Modul2/soal3/air";
    char zipfold[101] = "/Users/dentabramasta/Downloads/animal.zip";
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        //2A
        // buat darat
        char *createfolddarat[] = {"mkdir","-p", darat, NULL};
        norepeatfork("/bin/mkdir", createfolddarat);
        // execv("/bin/mkdir", darat)

        sleep(3);

        // buat air
        char *createfoldair[] = {"mkdir","-p", air, NULL};
        norepeatfork("/bin/mkdir", createfoldair);

        //2B
        char *unzipTheAnimal[] = {"unzip", "-q", zipfold, "-d", destination, "*.jpg", NULL};
        norepeatfork("/usr/bin/unzip", unzipTheAnimal);
    }
    else {
        while ((wait(&status))>0);
    }
}

// char* cutThePJG (char*s)
// {
//     int x;
//     int y;

//     char* new;

//     for (y = 0; s[y] != '\0'; y++);

//     // panjang dari string
//     x = y - 4 + 1;

//     if (x < 1)
//         return NULL;

//     new = (char*) malloc (x * sizeof(char));

//     for (y = 0; y < x - 1; y++)
//         new[y] = s[y];

//     new[y] = '\0';
//     return new;
// }


void movefiles () {
    pid_t child_id;
    int status;
    child_id = fork();
    char destiunzip[101] = "/Users/dentabramasta/Modul2/soal3/animal/";
    char Destair[101] = "/Users/dentabramasta/Modul2/soal3/air";
    char Destdar[101] = "/Users/dentabramasta/Modul2/soal3/darat";

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0)
    {
    DIR *directory;
    directory = opendir(destiunzip);
    if (directory != NULL)
    {
        struct dirent *folder;
        while ((folder = readdir(directory))!=NULL)
        {
            if((strcmp(folder->d_name, ".")== 0) ||(strcmp(folder->d_name, "..")== 0)) continue;

            if (folder -> d_type == DT_REG)
            {
                int counter=0;
                char *name = folder -> d_name;

                char result_1[100];
                char result_2[100];
                char result_3[100];
                char variable_path2[100];
                char variable_path3[100];

                char txt_location[100];
                char txt_path[100];
                char Nama_Hewan_in_txt[100];

                char nameOfAnimals[50];
                char typeOfAnimal[50];
                char habitatOfAnimal[50];

                strcpy(result_1, name);
                strcpy(variable_path2, destiunzip);
                strcpy(variable_path3, destiunzip);
                strcpy(result_2, name);
                strcpy(result_3, name);

                // strcat(destiunzip, folder->d_name);
                // strcat(Destdar, folder->d_name);

                // strcpy(result_1, destiunzip);
                // strcpy(result_2, Destdar);
                // char buffer[BUFSIZ] = { '\0' };
                // size_t len = 0 ;
                // FILE* in = fopen( result_1, "rb");
                // FILE* out = fopen( result_2, "wb");

                // if( in == NULL || out == NULL )
                // {
                //      perror( "An error occured while opening files!!!" ) ;
                //     in = out = 0 ;
                // }
                // else
                // {
                // while( (len = fread( buffer, BUFSIZ, 1, in)) > 0 )
                // {
                //     fwrite( buffer, BUFSIZ, 1, out ) ;
                // }

                //     fclose(in) ;
                //     fclose(out) ;
                //     remove(result_1);
                //             }
                // }
            char *Token = strtok(name, "_");
            while (Token != NULL)
            {
                if((strcmp(folder->d_name, ".")== 0) ||(strcmp(folder->d_name, "..")== 0)) continue;
                if (counter == 0)
                {
                    if (strcmp(Token, "air")==0)
                    {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                    norepeatfork("/bin/cp", moveTO);
                    }
                    else if (strcmp(Token, "darat")==0)
                {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                    norepeatfork("/bin/cp", moveTO);
                }

                }
                else if (counter == 1)
                {
                     if (strcmp(Token, "air")==0)
                    {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                    norepeatfork("/bin/cp", moveTO);
                    }
                    else if (strcmp(Token, "darat")==0)
                {
                //     strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                    norepeatfork("/bin/cp", moveTO);
                }
                }
                else if (counter == 2)
                {
                     if (strcmp(Token, "air")==0)
                    {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                    norepeatfork("/bin/cp", moveTO);
                    }

                    else if (strcmp(Token, "darat")==0)
                {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                    norepeatfork("/bin/cp", moveTO);
                }
                }

                // strcpy(txt_path, destiunzip);
                // strcpy(Nama_Hewan_in_txt, nameOfAnimals);

                // if (strcmp(Token, "air")==0)
                // {
                //     strcat(nameOfAnimals, ".jpg");
                //     strcat(variable_path2, result_2);

                //     char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                //     norepeatfork("/bin/cp", moveTO);
                // }
                // else if (strcmp(Token, "darat")==0)
                // {
                //     strcat(nameOfAnimals, ".jpg");
                //     strcat(variable_path2, result_2);

                //     char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                //     norepeatfork("/bin/cp", moveTO);
                // }

                counter++;
                Token = strtok(NULL, "_");
            }

            }

        }

    }
    }

    }

void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void deletefolder (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void delete (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        // Unzip();
        char destawal[101] = "/Users/dentabramasta/Modul2/soal3/";
        char destiunzip[101] = "/Users/dentabramasta/Modul2/soal3/animal";
        char Destair[101] = "/Users/dentabramasta/Modul2/soal3/air";
        char Destdar[101] = "/Users/dentabramasta/Modul2/soal3/darat";
        move(destiunzip, Destair, "*air*");
        move(destiunzip, Destdar, "*darat*");
        // char *rmdirec[] = {"rmdir","-p", destiunzip, NULL};
        // norepeatfork("/bin/rmdir", rmdirec);
        delete(destiunzip, "*.jpg*");

        delete(Destdar, "*bird*");
        // remove(destiunzip);

        struct dirent *folder;
        DIR *dir;
        while (folder = readdir(dir))
        {
            char pathFileLama[100] = "/Users/dentabramasta/Modul2";
            strcat(pathFileLama, "/soal3/animal/");
            strcat(pathFileLama, folder->d_name);
            remove(pathFileLama);
        }

    }
    else{
        wait(NULL);
    }
}


void create_list(char *place){
    FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "/Users/dentabramasta/Modul2/soal3/");
    strcat(path, place);
    strcat(path, "/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);

    // permission
    p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    // owner
    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    while((dp = readdir(dir))!= NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){

            // strcpy(path, "/Users/dentabramasta/Modul2/soal3/darat");
            // strcat(path, dp->d_name);

            char owner[NAME_MAX];
            char p_r[NAME_MAX];
            char p_w[NAME_MAX];
            char p_x[NAME_MAX];

            // owner
            struct passwd *pw = getpwuid(info.st_uid);
            if (pw != 0) strcpy(owner, pw->pw_name); //puts(pw->pw_name);

            // permission
            if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
            if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
            if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

            if(strstr(dp->d_name, "list") == 0){
                fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
            }
            // else if (strcmp(dp->d_name, dp->d_name)==0){
            //     continue;
            // }
        }
    }

    fclose(fp);
    return;
}

void data_list(){
    pid_t child = fork();
    int status;

    if (child < 0) exit(EXIT_FAILURE);

    if (child == 0){
        // create_list("air");
    }else{
        while((wait(&status)) > 0);
        create_list("air");
    }
}

int main () {
    Unzip();
    // movefiles();
    next();
    data_list();
    return 0;
}
```

## Explanation

before we start breaking down all of the function and codes. first I created fuction "norepeatfork" which aims to not create many children or create excessive forks. because we not allowed to use function mkdir instead using execv so we need to create a function for fork that not repeated.

```c
void norepeatfork (char execute[], char *variable_path[])
{
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0);
        return;
    }
}
```

### 3a. create directory called "air" and "darat" in “/home/[USER]/modul2/”

first I created char \*createfolderdarat/air it means to store all of the function such as mkdir. because we not allowed to use mkdir instead using execv or argv, so we called function norepeatfork and put the location of the function of mkdir and also insert the create folder in char. darat and air inside of {} it mean where is the location that we want to create.

```c
char Destair[101] = "/Users/dentabramasta/Modul2/soal3/air";
char Destdar[101] = "/Users/dentabramasta/Modul2/soal3/darat";

char *createfolddarat[] = {"mkdir","-p", darat, NULL};
norepeatfork("/bin/mkdir", createfolddarat);

char *createfoldair[] = {"mkdir","-p", air, NULL};
norepeatfork("/bin/mkdir", createfoldair);
```

Output:

![](https://i.ibb.co/TqmrPNr/Screen-Shot-2022-03-25-at-21-41-04.png)

### 3b. extract animal.zip

first we create the location where is the animal.zip located. then we create using execv "unzip" to unzip all of file that contain jpg (\*.jpg) the zip using norepeatfork function.

```c
char zipfold[101] = "/Users/dentabramasta/Downloads/animal.zip";
char *unzipTheAnimal[] = {"unzip", "-q", zipfold, "-d", destination, "*.jpg", NULL};
norepeatfork("/usr/bin/unzip", unzipTheAnimal);
```

Output:

![](https://i.ibb.co/wQsQHpZ/Screen-Shot-2022-03-25-at-21-41-17.png)

![](https://i.ibb.co/b2wvfN9/Screen-Shot-2022-03-25-at-21-42-24.png)

### 3c. separate the contents in the animal folder based on darat/air categories

in 3c I created function called move. inside of function move we have char \*source for the location of folder, destination for the destination of output, searchname is for search name that we want to move.

```c
void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}
```

this means we find from the source with the name that has air/darat and move to the destination

```c
char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
```

run the function

```c
 move(destiunzip, Destair, "*air*");
 move(destiunzip, Destdar, "*darat*");
```

Output:
![](https://i.ibb.co/ynL1vQL/Screen-Shot-2022-03-25-at-21-43-47.png)

![](https://i.ibb.co/8jM3JV2/Screen-Shot-2022-03-25-at-21-44-07.png)

### 3d. Delete bird

Same as previous (3c), it just change it from mv to rm which is rm(remove). first we find from source with name searchname, and no destination because we remove file not move files.

```c
void delete (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}
```

run the function

```c
delete(Destdar, "*bird*");
```

Output:
![](https://i.ibb.co/379dS51/Screen-Shot-2022-03-25-at-21-57-35.png)

### 3e. Create list.txt inside of air directory

we need to create variable that contain the destination of directory and also create the list.txt and open using fopen inside of fp.

```c
FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "/Users/dentabramasta/Modul2/soal3/");
    strcat(path, place);
    strcat(path, "/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);
```

because we need to use permission of the user so we need to get the permission using pwuid that i took from modul2 github.

```c
// permission
    p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    // owner
    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(info.st_uid);
    if (pw != 0) strcpy(owner, pw->pw_name);
```

then, we copy r, w, and x and print out as a name of files in list.txt

```c
// permission
if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

if(strstr(dp->d_name, "list") == 0){
    fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
}
```

Output :
![](https://i.ibb.co/km9Zk3H/Screen-Shot-2022-03-25-at-21-46-04.png)

## Revisions Made

- 3e rename files, iam forget to get the pwuid.
