#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>

typedef struct
{
    char title[50];
    int year;
} Data;
int compare (const void * a, const void * b)
{

  Data *dataA = (Data *)a;
  Data *dataB = (Data *)b;

  return ( dataA->year - dataB->year );
}
void dirmake(char *dest){
    char *argv[] = {"mkdir", "-p", dest, NULL};
    execv("/bin/mkdir", argv);
}
void unzipdrakor(char *zip, char *dest){
    char *unzip[] = {"unzip", zip, "-d", dest,"*.png", NULL};
    execv("/bin/unzip", unzip);
}
void norepeatfork (char execute[], char *variable_path[]) 
{ 
    int status; 
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0); 
    } 
}

//make a function that parsing string with underscore
char *parseString(char *s,int i){
    char *token;
    char *delimiter = ";";
    int var= 0;
    token = strtok(s,delimiter);
    while(var++ != i){
        token = strtok(NULL,delimiter);
    }
    if(strstr(token,".") == NULL)
        return token;
    return strtok(token,".");
}
void moveFile(char *src, char *dest){
    char *argv[] = {"mv", src, dest, NULL};
    execv("/bin/mv", argv);
}
void copyFile(char *src, char *dest){
    char *argv[] = {"cp", src, dest, NULL};
    execv("/bin/cp", argv);
}
void movegenre(char *directory){
    DIR *dir;
    pid_t child_id;
    int status;
    struct dirent *ent;
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            if(strstr(ent->d_name,".png") != NULL){
                char dest[300],dest2[300];
                char src[300];
                if(strstr(ent->d_name,"_") == NULL){
                    sprintf(src,"%s/%s",directory,ent->d_name);
                    char title[50];
                    strcpy(title,ent->d_name);
                    sprintf(dest,"%s/%s/%s",directory,parseString(title,2),ent->d_name);
                    child_id = fork();
                    if(child_id < 0)
                    {
                        exit(EXIT_FAILURE);
                    }
                    if(child_id == 0){
                        moveFile(src,dest);
                    }
                    else{
                        while ((wait(&status)) > 0);
                    }
                }
                else{
                    char *delimiter = "_";

                    sprintf(src,"%s/%s",directory,ent->d_name);
                    char title1[50];
                    char title2[50];
                    char *token = strtok(ent->d_name,delimiter);
                    char *token2 = strtok(NULL,delimiter);
                    strcpy(title1,token);
                    sprintf(dest,"%s/%s/%s.png",directory,parseString(title1,2),token);
                    strcpy(title2,token2);
                    sprintf(dest2,"%s/%s/%s",directory,parseString(title2,2),token2);
                    char *argv[] = {"cp", src, dest, NULL};
                    norepeatfork("/bin/cp", argv);
                    char *argv2[] = {"cp", src, dest2, NULL};
                    norepeatfork("/bin/cp", argv2);
                    char *argv3[] = {"rm", src, NULL};
                    norepeatfork("/bin/rm", argv3);
                }
            }
        }
    }
}

void removefile(){
    int status;
    pid_t child_id;
    child_id = fork();
    if(child_id == 0)
    {
        char *argv[] = {"rm", "/home/zufarrifqi/shift2/data.txt", NULL};
        execv("/bin/rm", argv);
    }
    else
    {
        ((wait(&status))>0);
        char *argv[] = {"rm", "/home/zufarrifqi/shift2/drakor/data.txt", NULL};
        execv("/bin/rm", argv);
    }
}
void makeFile(char* directory, Data data[],int count,char *genre){
    FILE *fptr;
    char direct[200];
    sprintf(direct,"%s",directory);
    chdir(direct);

    
    fptr = fopen("data.txt","w");
    //fprintf genre to data txt
    fprintf(fptr,"Kategori : %s\n",genre);
    //write data to data.txt
    qsort(data,count,sizeof(Data),compare);
    for(int i = 0; i < count; i++){ 
        fprintf(fptr,"\nnama : %s\n",data[i].title);
        fprintf(fptr,"rilis : tahun %d\n",data[i].year);
    }
    fclose(fptr);
    // removefile();
 }
void makeData(char *directory,char * genre){
    DIR *dir;
    pid_t child_id;
    int status;
    struct dirent *ent;
    int count = 0;
    Data data[100];
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            if(strstr(ent->d_name,".png") != NULL){
                char title[300];
                char src[300];
                char *delimiter = ";.";

                sprintf(src,"%s/%s",directory,ent->d_name);
                char *token = strtok(ent->d_name,delimiter);
                strcpy(data[count].title,token);
                // printf("%s\n",data[count].title);
                sprintf(title,"%s/%s.png",directory,token);
                token = strtok(NULL,delimiter);
                
                data[count].year = atoi(token);
                // printf("%d\n",data[count].year);
                // token = strtok(NULL,delimiter);
                // printf("%s\n",token);
                count++;
                child_id = fork();
                if(child_id < 0)
                {
                    exit(EXIT_FAILURE);
                }
                if(child_id == 0){
                    char *argv[] = {"mv",src,title, NULL};
                    execv("/bin/mv", argv);
                }
                else{
                    while ((wait(&status)) > 0);
                }
            }
            
        }
    }
    makeFile(directory,data,count,genre);
    memset(data,0,sizeof(Data)*100);
}
void changedir(char *directory){
    DIR *dir;
    struct dirent *ent;
    int files = 0;
    dir = opendir (directory);
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            char dest[300];
            sprintf(dest,"%s/%s",directory,ent->d_name);  
            makeData(dest,ent->d_name);
        }
    }
}
void mkdirgenre(char *directory){
    DIR *dir;
    struct dirent *ent;
    dir = opendir (directory);
    int status;
    pid_t child_id;
    char genredir[100],genre[10][100];
    if(dir != NULL){
        while ((ent = readdir(dir)) != NULL){
            child_id = fork();
            if(child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            if(child_id == 0){
                if(strstr(ent->d_name,"_") == NULL){
                    sprintf(genredir,"%s/%s",directory,parseString(ent->d_name,2));
                    dirmake(genredir);
                }
                else{
                    char *delimiter = "_";
                    char *token;
                    token = strtok(ent->d_name,delimiter);
                    child_id = fork();
                    if(child_id < 0)
                    {
                        exit(EXIT_FAILURE);
                    }
                    if(child_id == 0){
                        sprintf(genredir,"%s/%s",directory,parseString(token,2));
                        dirmake(genredir);
                    }

                }
            } 
            else{
                while ((wait(&status)) > 0);

            }
        }
        closedir(dir);
    }
    else perror ("Couldn't open the directory");
}

int main(){
    int status;
    char dest[100] = "/home/zufarrifqi/shift2/drakor";
    char zip[100] = "/home/zufarrifqi/drakor.zip";
    pid_t child_id;
    child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        dirmake(dest);
    }
    else{
        while ((wait(&status)) > 0);
        child_id = fork();
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }
        if(child_id == 0){
            unzipdrakor(zip,dest);
        }
        else{
            while ((wait(&status)) > 0);
            child_id = fork();
            if (child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            if(child_id == 0){
                mkdirgenre(dest);
            }
            else{
                while ((wait(&status)) > 0);
                movegenre(dest);
                changedir(dest);
                removefile();
                }

            }
        }
        
    }

