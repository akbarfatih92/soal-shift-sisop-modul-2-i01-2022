/*
Denta Bramasta Hidayat
5025201116
I01
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <syslog.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>

void norepeatfork (char execute[], char *variable_path[]) 
{ 
    int status; 
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0);
        return; 
    } 
}

void Unzip(){
    char destination[101] = "/Users/dentabramasta/Modul2/soal3";
    char darat[101] = "/Users/dentabramasta/Modul2/soal3/darat";
    char air[101] = "/Users/dentabramasta/Modul2/soal3/air";
    char zipfold[101] = "/Users/dentabramasta/Downloads/animal.zip";
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        //2A
        // buat darat
        char *createfolddarat[] = {"mkdir","-p", darat, NULL};
        norepeatfork("/bin/mkdir", createfolddarat);
        // execv("/bin/mkdir", darat)
        
        sleep(3);

        // buat air
        char *createfoldair[] = {"mkdir","-p", air, NULL};
        norepeatfork("/bin/mkdir", createfoldair);

        //2B
        char *unzipTheAnimal[] = {"unzip", "-q", zipfold, "-d", destination, "*.jpg", NULL};
        norepeatfork("/usr/bin/unzip", unzipTheAnimal);
    }
    else {
        while ((wait(&status))>0);
    }
}

// char* cutThePJG (char*s)
// {
//     int x;
//     int y;

//     char* new;

//     for (y = 0; s[y] != '\0'; y++);

//     // panjang dari string
//     x = y - 4 + 1;

//     if (x < 1)
//         return NULL;

//     new = (char*) malloc (x * sizeof(char));

//     for (y = 0; y < x - 1; y++)
//         new[y] = s[y];

//     new[y] = '\0';
//     return new;
// }


void movefiles () {
    pid_t child_id;
    int status;
    child_id = fork();
    char destiunzip[101] = "/Users/dentabramasta/Modul2/soal3/animal/";
    char Destair[101] = "/Users/dentabramasta/Modul2/soal3/air";
    char Destdar[101] = "/Users/dentabramasta/Modul2/soal3/darat";
    
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0)
    {
    DIR *directory;
    directory = opendir(destiunzip);
    if (directory != NULL)
    {
        struct dirent *folder;
        while ((folder = readdir(directory))!=NULL)
        {
            if((strcmp(folder->d_name, ".")== 0) ||(strcmp(folder->d_name, "..")== 0)) continue;

            if (folder -> d_type == DT_REG)
            {
                int counter=0;
                char *name = folder -> d_name;

                char result_1[100];
                char result_2[100]; 
                char result_3[100];
                char variable_path2[100];
                char variable_path3[100];

                char txt_location[100]; 
                char txt_path[100];
                char Nama_Hewan_in_txt[100];

                char nameOfAnimals[50];
                char typeOfAnimal[50];
                char habitatOfAnimal[50];

                strcpy(result_1, name);
                strcpy(variable_path2, destiunzip);
                strcpy(variable_path3, destiunzip);
                strcpy(result_2, name);
                strcpy(result_3, name);

                // strcat(destiunzip, folder->d_name);
                // strcat(Destdar, folder->d_name);

                // strcpy(result_1, destiunzip);
                // strcpy(result_2, Destdar);
                // char buffer[BUFSIZ] = { '\0' };
                // size_t len = 0 ;
                // FILE* in = fopen( result_1, "rb");
                // FILE* out = fopen( result_2, "wb");

                // if( in == NULL || out == NULL )
                // {
                //      perror( "An error occured while opening files!!!" ) ;
                //     in = out = 0 ;
                // }
                // else 
                // {
                // while( (len = fread( buffer, BUFSIZ, 1, in)) > 0 )
                // {
                //     fwrite( buffer, BUFSIZ, 1, out ) ;
                // }
                            
                //     fclose(in) ;
                //     fclose(out) ;
                //     remove(result_1);
                //             }
                // }
            char *Token = strtok(name, "_");
            while (Token != NULL)
            {
                if((strcmp(folder->d_name, ".")== 0) ||(strcmp(folder->d_name, "..")== 0)) continue;
                if (counter == 0)
                {
                    if (strcmp(Token, "air")==0)
                    {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                    norepeatfork("/bin/cp", moveTO);
                    }
                    else if (strcmp(Token, "darat")==0)
                {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                    norepeatfork("/bin/cp", moveTO);
                }
                    
                }
                else if (counter == 1)
                {
                     if (strcmp(Token, "air")==0)
                    {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                    norepeatfork("/bin/cp", moveTO);
                    }
                    else if (strcmp(Token, "darat")==0)
                {
                //     strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                    norepeatfork("/bin/cp", moveTO);
                }
                }
                else if (counter == 2)
                {
                     if (strcmp(Token, "air")==0)
                    {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                    norepeatfork("/bin/cp", moveTO);
                    }
                    
                    else if (strcmp(Token, "darat")==0)
                {
                    // strcat(nameOfAnimals, ".jpg");
                    strcat(variable_path2, result_2);

                    char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                    norepeatfork("/bin/cp", moveTO);
                }
                }
                
                // strcpy(txt_path, destiunzip);
                // strcpy(Nama_Hewan_in_txt, nameOfAnimals); 

                // if (strcmp(Token, "air")==0)
                // {
                //     strcat(nameOfAnimals, ".jpg");
                //     strcat(variable_path2, result_2);

                //     char *moveTO[] = {"cp", variable_path2, Destair, NULL};
                //     norepeatfork("/bin/cp", moveTO);
                // }
                // else if (strcmp(Token, "darat")==0)
                // {
                //     strcat(nameOfAnimals, ".jpg");
                //     strcat(variable_path2, result_2);

                //     char *moveTO[] = {"cp", variable_path2, Destdar, NULL};
                //     norepeatfork("/bin/cp", moveTO);
                // }
                
                counter++;
                Token = strtok(NULL, "_");
            }
            
            }
            
        }
        
    }
    }
    
    }

void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "cp", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void deletefolder (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void delete (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        // Unzip();
        char destawal[101] = "/Users/dentabramasta/Modul2/soal3/";
        char destiunzip[101] = "/Users/dentabramasta/Modul2/soal3/animal";
        char Destair[101] = "/Users/dentabramasta/Modul2/soal3/air";
        char Destdar[101] = "/Users/dentabramasta/Modul2/soal3/darat";
        move(destiunzip, Destair, "*air*");
        move(destiunzip, Destdar, "*darat*");
        // char *rmdirec[] = {"rmdir","-p", destiunzip, NULL};
        // norepeatfork("/bin/rmdir", rmdirec);
        // delete(destiunzip, "*.jpg*");
        
        delete(Destdar, "*bird*");
        remove(destiunzip);

        struct dirent *folder;
        DIR *dir;
        while (folder = readdir(dir))
        {
            char pathFileLama[100] = "/Users/dentabramasta/Modul2";
            strcat(pathFileLama, "/soal3/animal/");
            strcat(pathFileLama, folder->d_name);
            remove(pathFileLama);
        }
        
    }
    else{
        wait(NULL);
    }
}


void create_list(char *place){
    FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "/Users/dentabramasta/Modul2/soal3/");
    strcat(path, place);
    strcat(path, "/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);

    // permission
    p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    // owner
    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    while((dp = readdir(dir))!= NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            // strcpy(path, "/Users/dentabramasta/Modul2/soal3/darat");
            // strcat(path, dp->d_name);
            
            char owner[NAME_MAX];
            char p_r[NAME_MAX];
            char p_w[NAME_MAX];
            char p_x[NAME_MAX];
            
            // owner
            struct passwd *pw = getpwuid(info.st_uid);
            if (pw != 0) strcpy(owner, pw->pw_name); //puts(pw->pw_name);

            // permission
            if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
            if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
            if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

            if(strstr(dp->d_name, "list") == 0){
                fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
            }
            // else if (strcmp(dp->d_name, dp->d_name)==0){
            //     continue;
            // }
        }
    }

    fclose(fp);
    return;
}

void data_list(){
    pid_t child = fork();
    int status;

    if (child < 0) exit(EXIT_FAILURE);

    if (child == 0){
        // create_list("air");
    }else{
        while((wait(&status)) > 0);
        create_list("air");
    }
}

int main () {
    Unzip();
    // movefiles();
    next();
    data_list();
    return 0;
}